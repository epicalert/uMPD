/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROFILEMODEL_H
#define PROFILEMODEL_H

#include <QObject>
#include <QVariant>
#include <QAbstractListModel>
#include <QHash>
#include <QByteArray>
#include <QDebug>

class ProfileModel: public QAbstractListModel
{
    Q_OBJECT

public:
    enum theRoles {textRole= Qt::UserRole + 1, otherTextRole};
    
    ProfileModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void appendRow(int id, QString name, QString ip, int port, int timeout, QString pw);
    void removeRows(int position, int rows);
    
    void removeLastItem();
    void removeAllItems();

protected :
    QHash<int, QByteArray> roleNames() const;

private slots:
    
private:
    QList <int> m_idItems;
    QList <QString> m_nameItems;
    QList <QString> m_ipItems;
    QList <int> m_portItems;
    QList <int> m_timeoutItems;
    QList <QString> m_pwItems;
};

#endif
