/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "profilemodel.h"
//-------------------------------------------------------------------------------------------------

ProfileModel::ProfileModel(QObject *parent) : QAbstractListModel(parent)
{

}
//-------------------------------------------------------------------------------------------------
int ProfileModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_idItems.size();

}
//-------------------------------------------------------------------------------------------------
QVariant ProfileModel::data(const QModelIndex &index, int role) const
{
    if(m_idItems.size() != 0)
    {
        if (role == Qt::UserRole)
        {
            return QVariant(m_idItems[index.row()]);
        }
        if (role == Qt::UserRole+1)
        {
            return QVariant(m_nameItems[index.row()]);
        }
        if (role == Qt::UserRole+2)
        {
            return QVariant(m_ipItems[index.row()]);
        }
        if (role == Qt::UserRole+3)
        {
            return QVariant(m_portItems[index.row()]);
        }
        if (role == Qt::UserRole+4)
        {
            return QVariant(m_timeoutItems[index.row()]);
        }
        if (role == Qt::UserRole+5)
        {
            return QVariant(m_pwItems[index.row()]);
        }
        
        return QVariant();
    }
    return QVariant();

}

QHash<int,QByteArray> ProfileModel::roleNames() const {
  
  QHash<int, QByteArray> roles;
  roles[Qt::UserRole]="id";
  roles[Qt::UserRole+1]="name";
  roles[Qt::UserRole+2]="ip";
  roles[Qt::UserRole+3]="port";
  roles[Qt::UserRole+4]="timeout";
  roles[Qt::UserRole+5]="pw";
  
  return roles;
}

//-------------------------------------------------------------------------------------------------
void ProfileModel::appendRow(int id, QString name, QString ip, int port, int timeout, QString pw)
{
    qDebug() << "Profileintrag erstellt.";
    int lastElemPos = m_idItems.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_idItems << id;
    m_nameItems << name;
    m_ipItems << ip;
    m_portItems << port;
    m_timeoutItems << timeout;
    m_pwItems << pw;
    
    endInsertRows();
}
//-------------------------------------------------------------------------------------------------
void ProfileModel::removeRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row < rows; ++row) 
    {
        m_idItems.removeAt(position);
        m_nameItems.removeAt(position);
        m_ipItems.removeAt(position);
        m_portItems.removeAt(position);
        m_timeoutItems.removeAt(position);
        m_pwItems.removeAt(position);
    }

    endRemoveRows();    
}
//-------------------------------------------------------------------------------------------------
void ProfileModel::removeLastItem()
{
    if(m_idItems.size() > 0)
    {
        int lastElemPos = m_idItems.size() - 1;    
        removeRows(lastElemPos, 1);
    }
}

void ProfileModel::removeAllItems()
{
  while(m_idItems.size() > 0)
  {
    int lastElemPos = m_idItems.size() - 1;    
    removeRows(lastElemPos, 1);
  }
}
