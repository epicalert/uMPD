/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERDATABASE_H
#define PLAYERDATABASE_H

#include "playlistmodel.h"
#include "utils.h"
#include "connect.h"

class PlayerDatabase : public Connect
{
  Q_OBJECT
  
  Q_PROPERTY(PlayListModel *dirmodel READ dirmodel WRITE setDirmodel NOTIFY dirmodelChanged)
  Q_PROPERTY(PlayListModel *navmodel READ navmodel WRITE setNavmodel NOTIFY navmodelChanged)
  Q_PROPERTY(int navPos READ navPos WRITE setNavpos NOTIFY navposChanged)
  
public:
  PlayerDatabase(QObject *parent = 0);

  PlayListModel *dirmodel();
  void setDirmodel(PlayListModel *dirmodel);
  
  PlayListModel *navmodel();
  void setNavmodel(PlayListModel *navmodel);
  
  QString getLastFolder(QString path);

  void setNavpos(int navpos);
  int navPos() const;
  
  Q_INVOKABLE void getActDir(QString directory);
  Q_INVOKABLE void getActDir(int position, QString directory);
  Q_INVOKABLE bool update(void);
  
signals:
  void dirmodelChanged(PlayListModel *dirmodel);
  void navmodelChanged(PlayListModel *navmodel);
  void navposChanged(int navpos);

private slots:
  void onConnected();
  
private:
  int m_navpos = -1;
  
  PlayListModel *m_dirmodel = nullptr;
  PlayListModel *m_navmodel = nullptr;
};

#endif
