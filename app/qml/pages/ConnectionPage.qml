/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls.Suru 2.2
import "../components"

Page {
    id: settingsPage
    
    property bool isConnected: false
    property bool editing: false
    property bool needDisconnect: false
    property var profileId: -1
    property var tmpName: ""
    property var tmpAddress: ""
    property var tmpPort: 0
    property var tmpTimeout_ms: 0
    property var tmpPassword: ""
    
    header: PageHeader {
        id: pageHeader
        title: mpd.profile.isConnected ? mpd.profile.strProfile + ' ' + i18n.tr('connected') : i18n.tr('Connections')

        leadingActionBar.actions: [
            Action {
                visible: mpd.profile.isConnected
                iconName: "back"
                text: i18n.tr('Back')
                onTriggered: {
                    pageStack.pop()
                }                
            }
        ]
        
        trailingActionBar.actions: [
            Action {
                id: infoIcon
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                  push( Qt.resolvedUrl("./AboutPage.qml") )
                }
            },
            Action {
                id: addProfileIcon
                objectName: "addProfileIcon"
                text: i18n.tr("Add new profile")
                iconName: "list-add"
                onTriggered: {
                  tmpName= ""
                  tmpAddress= ""
                  tmpPort= ""
                  tmpTimeout_ms= ""
                  tmpPassword= ""
                  editing= false
                  PopupUtils.open(addProfile)
                }
            }
        ]
        
        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
    }

    Component {
        id: addProfile
	
        Dialog {
            id: addProfileDialog
            title: editing ? i18n.tr("Edit profile") : i18n.tr("Add new profile")
           
            RowLayout {
                spacing: units.gu(1)
                width: parent.width
                
                Label {
                    Layout.preferredWidth: (parent.width / 2 )
                    text: i18n.tr("Profile name:")
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
              
                TextField {
                    Layout.preferredWidth: (parent.width / 2 )
                    id: fieldProfileName
                    maximumLength : 20
                    horizontalAlignment: Text.AlignHCenter
                    placeholderText: i18n.tr("e.g. living room")
                    text: tmpName
                }
            }
            
            RowLayout {
                spacing: units.gu(1)
                
                Label {
                    Layout.preferredWidth: (parent.width / 2 )
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap                
                    text: i18n.tr('Ip Address:')
                }
                
                TextField {
                    id: fieldIpAdd
                    Layout.preferredWidth: (parent.width / 2 )
                    inputMethodHints: Qt.ImhDigitsOnly
                    maximumLength : 15
                    placeholderText: "127.0.0.1"
                    text: tmpAddress
                }
            }

            RowLayout {
                spacing: units.gu(1)
                
                Label {
                    Layout.preferredWidth: (parent.width / 2 )
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap 
                    text: i18n.tr('Port number:')
                }
                
                TextField {
                    id: fieldPort
                    Layout.preferredWidth: (parent.width / 2 )
                    inputMethodHints: Qt.ImhDigitsOnly
                    maximumLength : 5
                    placeholderText: "6600"
                    text: tmpPort
                }
            }
            
            RowLayout {
                spacing: units.gu(1)
                
                Label {
                    Layout.preferredWidth: (parent.width / 2 )
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap 
                    text: i18n.tr('Timeout (ms):')
                }
                
                TextField {
                    id: fieldTimeout
                    Layout.preferredWidth: (parent.width / 2 )
                    inputMethodHints: Qt.ImhDigitsOnly
                    maximumLength : 5
                    placeholderText: "0"
                    text: tmpTimeout_ms
                }
            }
            RowLayout {
                spacing: units.gu(1)
                
                Label {
                    Layout.preferredWidth: (parent.width / 2 )
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap 
                    text: i18n.tr('Password:')
                }

              TextField {
                    id: fieldPassword
                    Layout.preferredWidth: (parent.width / 2 )
                    maximumLength : 20
                    echoMode: TextInput.Password
                    placeholderText: "password"
                    text: tmpPassword
                }
            }
            
            Button {
                text: i18n.tr("save")
                color: UbuntuColors.green
                onClicked:
                {
                  if( editing ) {
                    editing = false
                    mpd.profile.editProfile(profileId, fieldProfileName.text, fieldIpAdd.text, fieldPort.text, fieldTimeout.text, fieldPassword.text, false)
                  } else {
                    mpd.profile.addProfile(fieldProfileName.text, fieldIpAdd.text, fieldPort.text, fieldTimeout.text, fieldPassword.text, false)
                  }
                  PopupUtils.close(addProfileDialog)
                }
            }

            Button {
                text: i18n.tr("save and connect")
                color: UbuntuColors.green
                onClicked:
                {
                    if( mpd.profile.isConnected ) mpd.profile.freeConnection()
                    if( editing ) {
                        editing = false
                        isConnected = mpd.profile.editProfile(profileId, fieldProfileName.text, fieldIpAdd.text, fieldPort.text, fieldTimeout.text, fieldPassword.text, true)
                        settings.profileIdxConnection = profileId
                    } else {
                        isConnected = mpd.profile.addProfile(fieldProfileName.text, fieldIpAdd.text, fieldPort.text, fieldTimeout.text, fieldPassword.text, true)
                    }
                    if( isConnected ) {
                        pageStack.push( Qt.resolvedUrl("./MainPage.qml") )
                    }
                    PopupUtils.close(addProfileDialog)
                }
            }
            
            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: {
                    editing = false
                    PopupUtils.close(addProfileDialog)
                }
            }
        }
    }
    
    //Seite
    ListView {
        id: profileView
        width: parent.width
        height: mpd.strError != "" ? (parent.height / 2) - pageHeader.height : parent.height - pageHeader.height
        anchors.top: pageHeader.bottom

        model: mpd.profile.profilemodel

        delegate: ListItem {

            height: modelLayout.height + (divider.visible ? divider.height : 0)

            ListItemLayout {
                id: modelLayout

                Icon {
                    width: units.gu(3)
                    height: units.gu(3)
                    color: Suru.foregroundColor
                    source: "file:///usr/share/icons/suru/status/scalable/stock_volume-max.svg"
                }

                Column {
                    id: column
                    Text {
                        color: Suru.foregroundColor
                        text: model.name
                        font.pointSize: units.gu(1.5)
                        font.bold: true
                    }
                    Text {
                        color: Suru.foregroundColor
                        text: "Ip: " + model.ip
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if( mpd.profile.isConnected ) mpd.profile.freeConnection()
                    if( mpd.profile.connectProfile(model.id) ) {
                        settings.profileIdxConnection = model.id
                        pageStack.push( Qt.resolvedUrl("./MainPage.qml") )
                    }
                }
            }

            leadingActions: ListItemActions {
                actions: [
                Action {
                    iconName: "delete"
                    text: i18n.tr("delete")
                    onTriggered: {
                        profileId = model.id
                        if( model.name == mpd.profile.strProfile ) needDisconnect = true
                        PopupUtils.open(removeProfile)
                    }
                }
                ]
            }

            trailingActions: ListItemActions {
                actions: [
                Action {
                    iconName: "edit"
                    text: i18n.tr("Edit")
                    onTriggered: {
                        tmpName= model.name
                        tmpAddress= model.ip
                        tmpPort= model.port
                        tmpTimeout_ms= model.timeout
                        tmpPassword= model.pw
                        profileId = model.id
                        editing= true
                        PopupUtils.open(addProfile)
                    }
                }
                ]
            }
        }
    }

    ErrorMsg {
      anchors.top: profileView.bottom

      width: parent.width
      height: (parent.height / 2)     

      visible: mpd.profile.strError != "" && !mpd.profile.noprofile ? true : false
      errorMsg: mpd.profile.strError
    }

    NoProfile {
        id: noProfileExists
        anchors.top: pageHeader.bottom
        anchors.topMargin: units.gu(10)

        visible: mpd.profile.noprofile

        width: parent.width
        height: parent.height - pageHeader.height - units.gu(10)
    }

    Component {
        id: removeProfile

        Dialog {
            id: removeProfileDialog
            title: i18n.tr("Remove profile")

            Label { 
                text: i18n.tr("Are you sure you want to delete the selected profile?")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                text: i18n.tr("delete")
                color: UbuntuColors.red
                onClicked:
                {
                    if(needDisconnect) mpd.profile.freeConnection()
                    needDisconnect = false
                    mpd.profile.deleteProfile(profileId)
                    PopupUtils.close(removeProfileDialog)
                }
            }

            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(removeProfileDialog)
            }
        }
    }
}
